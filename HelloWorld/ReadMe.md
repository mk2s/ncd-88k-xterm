## Hello World
Also see [blog post](https://m88k.org/posts/hello-world-on-the-ncd-xterm/)

the assembler command was **as hello.s** and the linker command was:

 `ld -o hello a.out --oformat=coff-m88kbcs -Ttext 4020000 -Tdata 8010000 -Tbss 8070000`
 
