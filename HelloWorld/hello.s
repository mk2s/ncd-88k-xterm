.text

# The folowing is required magic for NCD to like the image
or %r0, %r0, %r0
or %r0, %r0, %r0 /* no-op */
br start
.hword 0,1   /* 1 is for ignoring CRC16 */
.string "Xncd19C"
.int 0      /* CRC16 value would go here */

start: or.u %r2, %r0, hi16(L1)
or.u %r3, %r0, hi16(0x400e574) 
or   %r3, %r3, lo16(0x400e574)
# it would be a bsr.n
jmp.n %r3
or %r2, %r2, lo16(L1)
# we should not get here
or %r2, %r2, lo16(L3)

L1:
.ascii "hello world\n\0"

.data
L3:
.ascii "hello world\n\000"
.int 0x1234

.section .bss
.int 0x1234
